
/**
 * Firebase functions listeners
 */

/**
 * ---------------------------------------------------------------------------------------------------------------------
 * Core library
 * ---------------------------------------------------------------------------------------------------------------------
 */
const functions = require('firebase-functions');
const admin = require('firebase-admin');

/**
 * ---------------------------------------------------------------------------------------------------------------------
 * Variable initialization
 * ---------------------------------------------------------------------------------------------------------------------
 */
//Database initialization
var config = {

  }
admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: ''
});
const db = admin.database();
///ask-genius/request/{requestId}
//https://gullychat-3f64a.firebaseio.com/business/-KxlP4ipLPmAsrCeA8zm/images/-KxlP4lF22SLxuXrx3AT/imageUrl
exports.ratingCount = functions.database.ref('/rating/{businessId}')
    .onWrite((Datasnapshot,Context) => {
        var ratingArray=Datasnapshot.after._data;
        var ratingTotal = 0
        for(
            var i in ratingArray
        ){
            ratingTotal += ratingArray[i]
        }
        console.log("ratingTotal",ratingTotal)

        var ratingAvg = ratingTotal/Object.keys(ratingArray).length;
        console.log("ratingAvg",ratingAvg)
        var uid  = Datasnapshot.before._path.split("/")
        uid= uid[uid.length - 1];
        admin.database().ref("user").child(uid).update(
            JSON.parse(JSON.stringify({
                rating:Math.round(ratingAvg)
            }))
        )
});
